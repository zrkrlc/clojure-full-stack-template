(ns app.core
  (:require [reagent.core :as r]))

(defn ^:dev/after-load start
  []
  (r/render-component [:h1 "Hello World!"]
                      (.getElementById js/document "app")))

(defn ^:export main
  []
  (start))
