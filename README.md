# Clojure full-stack template
This is a minimal full-stack setup that also includes datomic (database) and kaocha (test runner).

## TODO
* [ ] Set up backend via `ring-jetty`
* [x] Set up frontend via `reagent` and `shadow-cljs`
* [x] Set up testing via `kaocha`
* [x] Set up `datomic`
* [ ] Set up Heroku deployment
* [ ] Switch to `datahike`
* [ ] Add Tailwind CSS support for quick prototyping
* [ ] Move to `client` and `server` namespaces without breaking `tests.edn`

## Setup
### Running the app
1) Install dependencies: `npm install shadow-cljs react react-dom create-react-class`
2) To start app: `clj -A:shadow-cljs watch app`
3) To run a separate REPL connected to shadow-cljs: `npx shadow-cljs cljs-repl app`
4) See __Setting up datomic__ below if you want to run it as well

### Running tests
NOTE: kaocha's configuration lives in `tests.edn`

1) `clj -A:test <test-id> --watch`, where `<test-id>` is either `unit-clj` or `unit-cljs` (omit it to run ALL tests)

### Setting up datomic
1) Copy your license key to `datomic/dev-transactor.properties`
2) In the `datomic` directory, run `bin/transactor dev-transactor.properties`

### Connecting vim-fireplace to the nREPL server of shadow-cljs
- See [LINK](https://github.com/tpope/vim-fireplace/issues/322#issuecomment-417461929)
1) While your app is running, open vim
2) Do `:FireplaceConnect <port>` (by default, `3333`) to connect to the server
3) If using CLJS, do `:Piggieback :app` to convert the running REPL to CLJS, then visit the app in your browser

### (OPTIONAL) Rename clj/cljs directories
For better namespacing, you may opt to rename the `clj`/`cljs` directories under `src` and `test` to more descriptive identifiers. For example, you may rename as follows: `clj`->`server`, `cljs`->`client`; moreover, you may also add a `cljc` (or e.g., `cljc`->`common`) directory for shared files.

Just remember to have your changes be reflected in `deps.edn`, `shadow-cljs.edn`, and `tests.edn`.

### (OPTIONAL) Building an uberjar
Running `clj -X:uberjar` will build a JAR file in the directory `builds`. Do `java -jar builds/app.jar` to run it.

## Installing packages
Add CLJS dependencies in `deps.edn` under `{:aliases {:shadow-cljs {:extra-deps ...}}}`.

